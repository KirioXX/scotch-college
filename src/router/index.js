import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/containers/Home'
import SuccessPage from '@/containers/ThankPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      props: (route) => ({
        image: route.query.image,
        version: route.query.version,
        course: route.query.course
      })
    },
    {
      path: '/thanks',
      name: 'Thanks',
      component: SuccessPage,
      props: (route) => ({
        course: route.query.course,
        name: route.query.name,
        email: route.query.email,
        country: route.query.country,
        phone: route.query.phone,
        qualification: route.query.qualification,
        location: route.query.location
      })
    }
  ]
})
